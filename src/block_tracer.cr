require "socket"
require "json"

# TODO: Write documentation for `BlockTracer`
module BlockTracer
  VERSION = "0.1.0"

  # //var offset = toHex(toAddress(log.stack.peek(0).toString(16)));
  # //var offset = toHex(toAddress(log.stack.peek(0).toString(16)));
  # this.addrCounts.ctx = ctx;
  JS="
  {
    addrCounts: {
      writes: {},
      reads: {},
      calls: {}
    },
    step: function(log, db) {
      var op = log.op.toString();
      switch(op) {
        case 'CALL':
        case 'CALLCODE':
        case 'STATICCALL':
        case 'DELEGATECALL':
          var addr = toHex(toAddress(log.stack.peek(1).toString(16)));

          // C par TX donc en fait un tableau suffit
          if (!this.addrCounts.calls[addr])
            this.addrCounts.calls[addr] = 1;
          else
            this.addrCounts.calls[addr]++;
          break;
        case 'SSTORE':
          var addr = toHex(log.contract.getAddress());
          if (!this.addrCounts.writes[addr])
            this.addrCounts.writes[addr] = 1;
          else
            this.addrCounts.writes[addr]++;
          break;
        case 'SLOAD':
          var addr = toHex(log.contract.getAddress());
          if (!this.addrCounts.reads[addr])
            this.addrCounts.reads[addr] = 1;
          else
            this.addrCounts.reads[addr]++;
          break;
        default:
          break;
      }
    },
    fault: function(log, db) {},
    result: function(ctx){
      return this.addrCounts;
    }
  }"

  JS2 = "
  {
    memrw: [],
    calls: 0,
    step: function(log, db) {
      var op = log.op.toString();
      switch(op) {
        case 'CALL':
        case 'CALLCODE':
        case 'STATICCALL':
        case 'DELEGATECALL':
          this.calls++;
          break;
        case 'SLOAD':
        case 'SSTORE':
          var contract = toHex(log.contract.getAddress());
          var addr = toHex(toAddress(log.stack.peek(0).toString(16)));
          this.memrw.push(contract+':'+addr);
          break;
        default:
          break;
      }
    },
    fault: function(log, db) {},
    result: function(ctx) {
      return { memrw: this.memrw, calls: this.calls };
    }
  }"

  def self.uglify(js)
    js.gsub(/^\s+/m, "")
  end

  def self.query_block(js, start, finish)
    uglified = self.uglify(JS2)

    fd = File.open "results_#{Time.now}.jsonl", "a+"

    (start..finish).each do |block_number|
      sock = UNIXSocket.new("/opt/chains/archivenode/geth.ipc")
      request = {
        id: 1,
        method: "debug_traceBlockByNumber",
        params: [
          "0x%x" % block_number,
          {
            tracer: uglified,
            reexec: finish+1,
            timeout: "600s"
          }
        ]
      }.to_json
      sock.puts request
      resp = sock.gets

      sock.close

      data = yield block_number, resp

      fd.puts data
    end

    fd.close
  end

  def self.mem(start, finish)
    self.query_block(JS2, start, finish) do |blocknum, resp|
      conflicts = Hash(String, Array(Int32)).new
      r = JSON.parse(resp.as(String))
      ncalls = 0

      if r["result"]?.nil?
        "#{blocknum}, 0, 0, 0"
      else
        r["result"].as_a.each_with_index do |result, txnum|
          memrw = result["result"]["memrw"].as_a
          ncalls += result["result"]["calls"].as_i64
          memrw.each do |a|
            access = a.as_s
            if conflicts.has_key?(access)
              conflicts[access].push(txnum) if conflicts[access][-1] != txnum
            else
              conflicts[access] = [txnum]
            end
          end
        end
  
        puts blocknum if blocknum % 1000 == 0
  
        nconflicts = conflicts.select { |k,v| v.size > 1 }.size
        # Store block number, number of txs, number of conflits and
        # number of calls in a vector.
        "#{blocknum}, #{r["result"].size}, #{nconflicts}, #{ncalls}"
      end
    end 
  end

  def calls(start, finish)
    query_block(JS, start, finish) do |block_number, resp|
      calls = Hash(String, Int64).new()

      r = JSON.parse(resp.as(String))
      next unless r.as_h.has_key?("result")
      r["result"].as_a.each do |result|
        c = result["result"]["writes"].as_h
        c.each do |k,v|
          if calls.has_key?(k)
            calls[k] += v.as_i64
          else
            calls[k] = v.as_i64
          end
        end
      end
      clashes = calls.select { |k, v| v > 1 }
      puts "#{block_number}, clashes: #{clashes.size}" if block_number % 1000 == 0
      resp.as(String)
    end
  end
end

# Can only start at block 1 which is annoying because it will shift
# everything by 1.
start = ARGV.size <= 1 ? 1 : ARGV[1].to_i64
finish = ARGV.size <= 0 ? 10000 : ARGV[0].to_i64
BlockTracer.mem(start, finish)

